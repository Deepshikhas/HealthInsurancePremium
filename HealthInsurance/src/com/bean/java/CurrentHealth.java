package com.bean.java;

public class CurrentHealth {

	private boolean Hypertension;
	private boolean Bloodpressure;
	public CurrentHealth(boolean hypertension, boolean bloodpressure, boolean bloodsugar, boolean overweight) {
		super();
		Hypertension = hypertension;
		Bloodpressure = bloodpressure;
		Bloodsugar = bloodsugar;
		Overweight = overweight;
	}
	private boolean Bloodsugar;
	private boolean Overweight;
	
	public boolean isHypertension() {
		return Hypertension;
	}
	public void setHypertension(boolean hypertension) {
		Hypertension = hypertension;
	}
	public boolean isBloodpressure() {
		return Bloodpressure;
	}
	public void setBloodpressure(boolean bloodpressure) {
		Bloodpressure = bloodpressure;
	}
	public boolean isBloodsugar() {
		return Bloodsugar;
	}
	public void setBloodsugar(boolean bloodsugar) {
		Bloodsugar = bloodsugar;
	}
	public boolean isOverweight() {
		return Overweight;
	}
	public void setOverweight(boolean overweight) {
		Overweight = overweight;
	}
	
}
