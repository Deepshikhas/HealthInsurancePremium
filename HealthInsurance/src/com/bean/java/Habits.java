package com.bean.java;

public class Habits {

	private boolean Smoking;
	private boolean Alcohol;
	private boolean Daily_exercise;
	private boolean Drugs;
	public boolean isSmoking() {
		return Smoking;
	}
	public Habits(boolean smoking, boolean alcohol, boolean daily_exercise, boolean drugs) {
		super();
		this.Smoking = smoking;
		this.Alcohol = alcohol;
		this.Daily_exercise = daily_exercise;
		this.Drugs = drugs;
	}
	public void setSmoking(boolean smoking) {
		Smoking = smoking;
	}
	public boolean isAlcohol() {
		return Alcohol;
	}
	public void setAlcohol(boolean alcohol) {
		Alcohol = alcohol;
	}
	public boolean isDaily_exercise() {
		return Daily_exercise;
	}
	public void setDaily_exercise(boolean daily_exercise) {
		Daily_exercise = daily_exercise;
	}
	public boolean isDrugs() {
		return Drugs;
	}
	public void setDrugs(boolean drugs) {
		Drugs = drugs;
	}
	
	}
