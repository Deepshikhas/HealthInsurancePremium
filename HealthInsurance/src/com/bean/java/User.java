package com.bean.java;

public class User {

	private String name;
	private String gender;
	private int age;
	private CurrentHealth curhealth;
	private Habits habits;
	
	public User(String name,String gender,int age,CurrentHealth curhealth,Habits habits)
	{
		this.name=name;
		this.gender=gender;
		this.age=age;
		this.curhealth=curhealth;
		this.habits=habits;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CurrentHealth getCurhealth() {
		return curhealth;
	}
	public void setCurhealth(CurrentHealth curhealth) {
		this.curhealth = curhealth;
	}
	public Habits getHabits() {
		return habits;
	}
	public void setHabits(Habits habits) {
		this.habits = habits;
	}
	
}
