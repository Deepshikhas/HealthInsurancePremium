package com.client.java;

import java.util.Scanner;
import com.bean.java.CurrentHealth;
import com.bean.java.Habits;
import com.bean.java.User;
import com.dao.java.HealthPremiumCalcutor;

public class HealthPremium {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner sc=new Scanner(System.in);
		
		int healthcount=0;
		double totalPremium=0;
		System.out.println("enter the details of user");
		System.out.println("enter name,gender,age");
		String name=sc.next();
		String gender=sc.next();
		int age=sc.nextInt();
		System.out.println("enter true or false for below values");
		System.out.println("do you have hypertension?");
		boolean Hypertension=sc.nextBoolean();
		System.out.println("bloodpressure?");
		boolean Bloodpressure=sc.nextBoolean();
		System.out.println("bloodsugar?");
		 boolean Bloodsugar=sc.nextBoolean();
		 System.out.println("overweight?");
		 boolean Overweight=sc.nextBoolean();
		 System.out.println("smoking?");
		 boolean Smoking=sc.nextBoolean();
		 System.out.println("alcohol?");
		 boolean Alcohol=sc.nextBoolean();
		 System.out.println("daily exercise?");
		 boolean Daily_exercise=sc.nextBoolean();
		 System.out.println("drugs?");
		 boolean Drugs=sc.nextBoolean();
		 CurrentHealth curhealth=new CurrentHealth(Hypertension, Bloodpressure, Bloodsugar, Overweight);
		 Habits habits=new Habits(Smoking,Alcohol,Daily_exercise,Drugs);
		 User user=new User(name, gender,age,curhealth,habits);
		 HealthPremiumCalcutor healthPremiumCalcutor=new HealthPremiumCalcutor();
		 
		 if(user.getCurhealth().isBloodpressure())
		 {
			 healthcount+=healthcount;
		 }
		
		 if(user.getCurhealth().isBloodsugar())
		 {
			 healthcount+=healthcount;
		 }
		
		 if(user.getCurhealth().isHypertension())
			 
		 {
			 healthcount+=healthcount;
		 }
		
		 if(user.getCurhealth().isOverweight())
		 {
			 healthcount+=healthcount;
		 }
		
		 double current_health_premium=healthPremiumCalcutor.getCurrentHealthPremium(healthcount);
		 int habitscount=0;
		 
		 if(user.getHabits().isAlcohol())
		 {
			 habitscount+=habitscount;
			 
		 }
		 if(user.getHabits().isDrugs())
		 {
			 habitscount+=habitscount;
			 
		 }
		 if(user.getHabits().isSmoking())
		 {
			 habitscount+=habitscount;
			 
		 }
		 
		 double habit_premium=healthPremiumCalcutor.getPremiumOnHabits(user,habitscount);
		
		 totalPremium=healthPremiumCalcutor.calculatePremium(user,current_health_premium,habit_premium);
		
		System.out.println("the total premium is:" +totalPremium);
		
	}
}
