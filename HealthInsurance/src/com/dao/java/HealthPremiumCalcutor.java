package com.dao.java;

import com.bean.java.User;

public class HealthPremiumCalcutor {

	 private static int base_premium=5000;
	public  double calculatePremium(User user,double current_health_premium,double habit_premium)
	{
		
		double agepremium=0,genderpremium=0;
		
		
		if(user.getAge()<18)
		{
			agepremium=base_premium;
		}
		else if(user.getAge()>=18 && user.getAge()<=40)
		{
			
			agepremium=base_premium+( 0.1*base_premium);
		}
		else if(user.getAge()>40)
		{
			agepremium=base_premium+( 0.2*base_premium);
		}
		
		if(user.getGender().equalsIgnoreCase("Female") || user.getGender().equalsIgnoreCase("others"))
		{
			genderpremium=agepremium+(0.02*base_premium);
		}
		else
			genderpremium=agepremium;
			
		
		
		double total_premium=genderpremium+current_health_premium+habit_premium;
		return total_premium;
	}
	
	
	public double getCurrentHealthPremium(int number_of_disease)
	{
		int base_premium=5000;
		double current_health_premium=0;
		current_health_premium=number_of_disease*0.01*base_premium;
		return current_health_premium;
	}
	
	public double getPremiumOnHabits(User user,int num_of_badhabits)
	{
			double habitPremium=0;
		if(user.getHabits().isDaily_exercise())
		{
			habitPremium=base_premium-0.03*base_premium;
		}
		
		habitPremium=habitPremium+num_of_badhabits*0.03*base_premium;
		
		return habitPremium;
	}
}
